<?php

require_once 'classes/oauth.php';
require_once 'functions/header.php';
require_once 'functions/db.php';
require_once 'functions/json.php';

$oauthObj = new myOauth(); // Instantiate the main OAuth object.
if (!empty($_GET['access_token'])) {
	$oauthObj->access_token = $_GET['access_token'];
} elseif (!empty($_POST['access_token'])) {
	$oauthObj->access_token = $_POST['access_token'];
} elseif ($oauthObj->access_token = getBearerToken()) {
	// Everything is good. Keep moving.
} else {
	echo json_encode(array('error' => 'Connection Refused: No access token provided.'));
	die;
}

if (!empty($oauthObj->access_token) && $tokenObj = $oauthObj->getAccessTokenFromAccessToken($oauthObj->access_token)) {
	$today = date("Y-m-d H:i:s");
	$expire = $tokenObj[0]['expires'];
	$today_time = strtotime($today);
	$expire_time = strtotime($expire);
	if ($expire_time > $today_time) {
		// Continue on to the API code.
	} else {
		echo json_encode(array('error' => 'Connection Refused: Access token expired.'));
		die;
	}
} elseif (!empty($oauthObj->access_token)) {
	echo json_encode(array('error' => 'Connection Refused: Invalid access token.'));
	die;
}

$connection = dbConnect('taboplsk_dbtabo');

$request_method=$_SERVER["REQUEST_METHOD"];
switch($request_method) {
	case 'GET':
		// Retrieve Timesheets
		parse_str(file_get_contents("php://input"),$post_vars);
		if(!empty($post_vars["id"])) {
			$timesheet_id=intval($post_vars["id"]);
			get_timesheets($timesheet_id);
		} elseif (!empty($_POST["id"])) {
			$timesheet_id=intval($_POST["id"]);
			get_timesheets($timesheet_id);
		} elseif (!empty($_GET["id"])) {
			$timesheet_id=intval($_GET["id"]);
			get_timesheets($timesheet_id);
		} else {
			get_timesheets();
		}
		break;
	case 'POST':
		// Insert Timesheets
		$post_vars = file_get_contents("php://input"); // Check to see if data is coming from an input file.
		if (!empty($post_vars)) {
			if (isJSON($post_vars)) { // If JSON, then data is coming from a file and must be processed as JSON.
				$insertion_array = array();
				$post_vars = json_decode($post_vars);
				foreach($post_vars->data as $obj) {
					foreach($obj as $field => $value) {
						$insertion_array[$field] = $value;
					}
					if (!empty($insertion_array)) {
						insert_timesheet($insertion_array);
					}
				}
			} else { // Not JSON, so assume the data is coming from the command line, and parse it.
				parse_str($post_vars, $data);
				insert_timesheet($data);
			}
		} elseif (!empty($_POST)) {
			insert_timesheet($_POST);
		} elseif (!empty($_GET)) {
			insert_timesheet($_GET);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'No timesheet data provided.'
			);
			header('Content-Type: application/json');
			echo json_encode($response);
		}
		break;
	case 'PUT':
		// Update timesheet
		$post_vars = file_get_contents("php://input"); // Check to see if data is coming from an input file.
		if (!empty($post_vars)) {
			if (isJSON($post_vars)) { // If JSON, then data is coming from a file and must be processed as JSON.
				$update_array = array();
				$post_vars = json_decode($post_vars);
				foreach($post_vars->data as $obj) {
					foreach($obj as $field => $value) {
							$update_array[$field] = $value;
					}
					if (!empty($update_array)) {
						update_timesheet($update_array);
					}
				}
			} else { // Not JSON, so assume the data is coming from the command line, and parse it.
				parse_str($post_vars, $data);
				update_timesheet($data);
			}
		} elseif (!empty($_POST)) {
			update_timesheet($_POST);
		} elseif (!empty($_GET)) {
			update_timesheet($_GET);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'No timesheet ID provided.'
			);
			header('Content-Type: application/json');
			echo json_encode($response);
		}
		break;
	case 'DELETE':
		// Delete timesheet
		$post_vars = file_get_contents("php://input"); // Check to see if data is coming from an input file.
		if (!empty($post_vars)) {
			if (isJSON($post_vars)) { // If JSON, then data is coming from a file and must be processed as JSON.
				$update_array = array();
				$post_vars = json_decode($post_vars);
				foreach($post_vars->data as $obj) {
					foreach($obj as $field => $value) {
							$update_array[$field] = $value;
					}
					if (!empty($update_array)) {
						delete_timesheet($update_array);
					}
				}
			} else { // Not JSON, so assume the data is coming from the command line, and parse it.
				parse_str($post_vars, $data);
				delete_timesheet($data);
			}
		} elseif (!empty($_POST)) {
			delete_timesheet($_POST);
		} elseif (!empty($_GET)) {
			delete_timesheet($_GET);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'No timesheet ID provided.'
			);
			header('Content-Type: application/json');
			echo json_encode($response);
		}
		break;
	default:
		// Invalid Request Method
		header("HTTP/1.0 405 Method Not Allowed");
		break;
}

/* This function gets timesheet data from the database. It can get either a single timesheet based on a timesheet ID or 
   all of the timesheets if no ID is provided. */
function get_timesheets($timesheet_id=0) {
	global $connection;
	try {
		$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		if($timesheet_id != 0) {
			$stmt = $connection->prepare("SELECT * FROM Timesheets WHERE ID = :id LIMIT 1");
			$stmt->bindParam(':id', $timesheet_id);
		} else {
			$stmt = $connection->prepare("SELECT * FROM Timesheets");
		}
		
		$response=array();
		if ($stmt->execute()) {
			$response = $stmt->fetchAll();
			header('Content-Type: application/json');
			echo json_encode($response);
		}
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}

/* This function adds timesheet data to the database. */
function insert_timesheet($data) {
	global $connection;
	try {
		$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$sql = 'INSERT INTO Timesheets (';
		
		foreach($data as $key => $value) { // Build the column list for the INSERT statement.
			if ($key != 'id') {
				$sql .= $key . ',';
			}
		}
		$sql = rtrim($sql,',');
		$sql .= ') VALUES (';

		foreach($data as $key => $value) { // Build the value list for the INSERT statement.
			if ($key != 'id') {
				$sql .= ':' . $key . ',';
			}
		}
		$sql = rtrim($sql,',');
		$sql .= ')';
		
		$stmt = $connection->prepare($sql);
										
		foreach($data as $key => $value) { // Build the variable bind list for the SQL statement.
			if ($key != 'id') {
				$stmt->bindParam(':'.$key, $data[$key]);
			}
		}
				
		if($stmt->execute()) {
			$response=array(
				'status' => 1,
				'status_message' =>'Timesheet Added Successfully.'
			);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'Timesheet Addition Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}

/* This function updates timesheet data in the database. */
function update_timesheet($data) {
	global $connection;

	try {
		$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$sql = "UPDATE Timesheets SET ";
		
		foreach($data as $key => $value) { // Build the column = :key list for the UPDATE statement.
			if ($key != 'timesheet_id' && $key != 'id') {
				$sql .= $key . ' = ' . ':' . $key . ',';
			}
		}
		$sql = rtrim($sql,',');
		$sql .= " WHERE id = :id";
		
		$stmt = $connection->prepare($sql);
										
		foreach($data as $key => $value) { // Build the variable bind list for the SQL statement.
			$stmt->bindParam(':'.$key, $data[$key]);
		}
		
		if($stmt->execute()) {
			$response=array(
				'status' => 1,
				'status_message' =>'Timesheet Updated Successfully.'
			);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'Timesheet Update Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}

/* This function deletes timesheet data from the database */
function delete_timesheet($data) {
	global $connection;
	try {
		$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt = $connection->prepare("DELETE FROM Timesheets WHERE ID = :id");
		$stmt->bindParam(':id', $data["id"]);
		if($stmt->execute()) {
			$response=array(
				'status' => 1,
				'status_message' =>'Timesheet Deleted Successfully.'
			);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'Timesheet Deletion Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}
