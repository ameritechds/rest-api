<?php

class myOauth {
	public $access_token;
	public $refresh_token;
	public $client_id;
	public $user_id;
	public $password;
	public $client_secret;
	public $auth_code;
	
	/* The dbConnect() method establishes a database connection that will be used to get, post, update, and delete
	   information from the database. (Will probably need to be moved to a separate file for security reasons.)
	   The method returns a database connection object. */
	public function dbConnect() {
		$user_id = 'root';
		$password = '';
		$host = 'localhost';
		$db = 'oauth2_db';
		$connection = new PDO("mysql:dbname=$db;host=$host", $user_id, $password);
		return $connection;
	}
	
	/* The verifyLogonCreds() method validates the user's logon credentials (i.e. username and password). The method
	   returns true if the credentials check out, and false if they do not. */
	public function verifyLogonCreds() {
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("SELECT user_id FROM oauth_clients WHERE user_id = :user_id AND password = :password LIMIT 1");
			$stmt->bindParam(':user_id', $this->user_id);
			$stmt->bindParam(':password', $this->password);
			
			$response=array();
			if ($stmt->execute()) {
				$response = $stmt->fetchAll();
				$dbConn = null;
				if (!empty($response[0]['user_id']) && $response[0]['user_id'] == $this->user_id) {
					return true;
				} else {
					return false;
				}
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The verifyClientIDAndSecret() method validates the client ID and client secret combination. The method returns true
	   if the combination checks out and false if it does not. */
	public function verifyClientIDAndSecret() {
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("SELECT user_id FROM oauth_clients WHERE client_id = :client_id AND client_secret = :client_secret LIMIT 1");
			$stmt->bindParam(':client_id', $this->client_id);
			$stmt->bindParam(':client_secret', $this->client_secret);
			
			$response=array();
			if ($stmt->execute()) {
				$response = $stmt->fetchAll();
				$dbConn = null;
				if (!empty($response[0]['user_id'])) {
					return true;
				} else {
					return false;
				}
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The verifyClientIDAndAuthCode() method validates the client ID and authorization code combination. The method returns
	   true if the combination checks out, and false if it does not. */
	public function verifyClientIDAndAuthCode() {
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("SELECT authorization_code, expires FROM oauth_authorization_codes WHERE authorization_code = :auth_code AND client_id = :client_id LIMIT 1");
			$stmt->bindParam(':auth_code', $this->auth_code);
			$stmt->bindParam(':client_id', $this->client_id);
			
			$response=array();
			if ($stmt->execute()) {
				$response = $stmt->fetchAll();
				$dbConn = null;
				if (!empty($response[0]['authorization_code'])) {
					$today = date("Y-m-d H:i:s");
					$expire = $response[0]['expires'];
					$today_time = strtotime($today);
					$expire_time = strtotime($expire);
					if ($expire_time > $today_time) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The verifyRefreshToken() method validates a particular refresh token. The method returns true if the refresh token
	   is valid and false if it is not. */
	public function verifyRefreshToken() {
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("SELECT expires FROM oauth_refresh_tokens WHERE refresh_token = :refresh_token LIMIT 1");
			$stmt->bindParam(':refresh_token', $this->refresh_token);
			
			$response=array();
			if ($stmt->execute()) {
				$response = $stmt->fetchAll();
				$dbConn = null;
				if (!empty($response[0]['expires'])) {
					$today = date("Y-m-d H:i:s");
					$expire = $response[0]['expires'];
					$today_time = strtotime($today);
					$expire_time = strtotime($expire);
					if ($expire_time > $today_time) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* Generates an access token. */
	public function generateAccessToken() {
		$this->access_token = sha1(random_bytes(40), false);
	}
	
	/* Generates a refresh token. */
	public function generateRefreshToken() {
		$this->refresh_token = sha1(random_bytes(40), false);
	}

	/* Generates a client ID. */
	public function generateClientID() {
		$this->client_id = sha1(random_bytes(40), false);
	}
	
	/* Generates a client secret. */
	public function generateClientSecret() {
		$this->client_secret = sha1(random_bytes(40), false);
	}
	
	/* Generates an authorization code. */
	public function generateAuthCode() {
		$this->auth_code = sha1(random_bytes(40), false);
	}
	
	/* The getAccessTokenFromClientID() method retrieves an access token and its expiration date from the database
	   based on the client ID. It returns the database fields (i.e. access_token and expires) if the token is found,
	   and it returns false if the token is not found. */
	public function getAccessTokenFromClientID($client_id) {
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("SELECT access_token, expires FROM oauth_access_tokens WHERE client_id = :id LIMIT 1");
			$stmt->bindParam(':id', $client_id);
			
			$response=array();
			if ($stmt->execute()) {
				$response = $stmt->fetchAll();
				$dbConn = null;
				if (!empty($response)) {
					return $response;
				} else {
					return false;
				}
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The getAccessTokenFromAccessToken() method retrieves and access token and its expiration date from the database
	   based on the access token provided by the client. It returns the database fields (i.e. access token and expires)
	   if the token is found, and false if the token is not found. */
	public function getAccessTokenFromAccessToken($access_token) {
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("SELECT access_token, expires FROM oauth_access_tokens WHERE access_token = :access_token LIMIT 1");
			$stmt->bindParam(':access_token', $access_token);
			
			$response=array();
			if ($stmt->execute()) {
				$response = $stmt->fetchAll();
				$dbConn = null;
				if (!empty($response)) {
					return $response;
				} else {
					return false;
				}
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The getClientSecret() retrieves the client secret from the database based on the client ID provided by the client.
	   It returns the secret if it is found, and false if it is not. */
	public function getClientSecret($client_id) {
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("SELECT client_secret FROM oauth_clients WHERE client_id = :id LIMIT 1");
			$stmt->bindParam(':id', $client_id);
			
			$response=array();
			if ($stmt->execute()) {
				$response = $stmt->fetchAll();
				$dbConn = null;
				if (!empty($response[0]['client_secret'])) {
					return $response[0]['client_secret'];
				} else {
					return false;
				}
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The setAccessToken() method inserts a new access token into the database. The token is stored with the
	   client ID and the expiration date/time. The method returns true if the insert is successful and false
	   if it is not. */
	public function setAccessToken() {
		$expires = date("Y-m-d H:i:s", strtotime('+3 days'));
		
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("INSERT INTO oauth_access_tokens (access_token,
																		client_id,
																		expires)
															VALUES	  (:access_token,
																		:client_id,
																		:expires)");
			$stmt->bindParam(':access_token', $this->access_token);
			$stmt->bindParam(':client_id', $this->client_id);
			$stmt->bindParam(':expires', $expires);
			
			if ($stmt->execute()) {
				$dbConn = null;
				return true;
			} else {
				return false;
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The setRefreshToken() method inserts a new refresh token into the database. The token is stored with the
	   client ID and the expiration date/time. The method returns true if the insert is successful and false
	   if it is not. */
	public function setRefreshToken() {
		$expires = date("Y-m-d H:i:s", strtotime('+3 days'));
		
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("INSERT INTO oauth_refresh_tokens (refresh_token,
																		client_id,
																		expires)
															VALUES	  (:refresh_token,
																		:client_id,
																		:expires)");
			$stmt->bindParam(':refresh_token', $this->refresh_token);
			$stmt->bindParam(':client_id', $this->client_id);
			$stmt->bindParam(':expires', $expires);
			
			if ($stmt->execute()) {
				$dbConn = null;
				return true;
			} else {
				return false;
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The setAuthCode() method inserts a new authorization code into the database. The code is stored with the
	   client ID and the expiration date/time. The method returns true if the insert is successful and false
	   if it is not. */
	public function setAuthCode() {
		$expires = date("Y-m-d H:i:s", strtotime('+1 minute'));
		
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("INSERT INTO oauth_authorization_codes (authorization_code,
																			client_id,
																			expires)
															VALUES	  		(:authorization_code,
																			:client_id,
																			:expires)");
			$stmt->bindParam(':authorization_code', $this->auth_code);
			$stmt->bindParam(':client_id', $this->client_id);
			$stmt->bindParam(':expires', $expires);
			
			if ($stmt->execute()) {
				$dbConn = null;
				return true;
			} else {
				return false;
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The setClientID() method updates the client ID in the database. The method returns true if the update is successful and false
	   if it is not. */
	public function setClientID() {
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("UPDATE oauth_clients
										SET client_id = :client_id
										WHERE user_id = :user_id");
			$stmt->bindParam(':client_id', $this->client_id);
			$stmt->bindParam(':user_id', $this->user_id);
			
			if ($stmt->execute()) {
				$dbConn = null;
				return true;
			} else {
				return false;
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The setClientSecret() method updates the client secret in the database. The method returns true if the update is successful and false
	   if it is not. */
	public function setClientSecret() {
		
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("UPDATE oauth_clients
										SET client_secret = :client_secret
										WHERE user_id = :user_id");
			$stmt->bindParam(':client_secret', $this->client_secret);
			$stmt->bindParam(':user_id', $this->user_id);
			
			if ($stmt->execute()) {
				$dbConn = null;
				return true;
			} else {
				return false;
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
	
	/* The deleteAccessToken() method deletes an access token from the database. It returns true if the deletion
	   is successful and false if it is not. (Currently unused.) */
	public function deleteAccessToken($access_token) {
		$dbConn = $this->dbConnect();
		
		try {
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $dbConn->prepare("DELETE FROM oauth_access_tokens WHERE access_token = :access_token");
			$stmt->bindParam(':access_token', $access_token);
			
			if ($stmt->execute()) {
				$dbConn = null;
				return true;
			} else {
				return false;
			}
		} catch(PDOException $e) {
			echo "Error: " . $e->getMessage();
		}
		
		return false;
	}
}