<?php

require_once 'classes/oauth.php';
require_once 'functions/header.php';
require_once 'functions/api.php';
require_once 'vendor/paragonie/random_compat/lib/random.php';

$tokenObj = "";
$today = "";
$expire = "";
$today_time = "";
$expire_time = "";
$access_token = "";

if (!empty($_GET)) {
	$inputdata = $_GET;
} elseif (!empty($_POST)) {
	$inputdata = $_POST;
} else {
	parse_str(file_get_contents("php://input"),$inputdata);
}

if (!empty($inputdata)) {
	$oauthObj = new myOauth(); // Instantiate the main OAuth object.
	
	if (!empty($inputdata['user_id'])) {
		$oauthObj->user_id = $inputdata['user_id'];
	}
	if (!empty($inputdata['password'])) {
		$oauthObj->password = sha1($inputdata['password']);
	}
	if (!empty($inputdata['client_id'])) {
		$oauthObj->client_id = $inputdata['client_id'];
	}
	if (!empty($inputdata['client_secret'])) {
		$oauthObj->client_secret = $inputdata['client_secret'];
	}
	if (!empty($inputdata['auth_code'])) {
		$oauthObj->auth_code = $inputdata['auth_code'];
	}
	if (!empty($inputdata['access_token'])) {
		$oauthObj->access_token = $inputdata['access_token'];
	} elseif ($access_token = getBearerToken()) {
		$oauthObj->access_token = $access_token;
	}
	if (!empty($inputdata['refresh_token'])) {
		$oauthObj->refresh_token = $inputdata['refresh_token'];
	}
	
	/* This section of code checks to see if the user passed in a valid access token. If so, it allows direct access to the
	   API. If not, it returns an invalid access token error. */
	if (!empty($oauthObj->access_token) && $tokenObj = $oauthObj->getAccessTokenFromAccessToken($oauthObj->access_token)) {
		$today = date("Y-m-d H:i:s");
		$expire = $tokenObj[0]['expires'];
		$today_time = strtotime($today);
		$expire_time = strtotime($expire);
		if ($expire_time > $today_time) {
			// Forward on to API.
			if (!empty($inputdata['api']) && $inputdata['api'] == 'customers') {
				$url = "http://localhost/api/customers/";
				// Forwarding on to the API.
				sendToAPI($inputdata, $url, $_SERVER["REQUEST_METHOD"], $oauthObj->access_token);
			} elseif (!empty($inputdata['api']) && $inputdata['api'] == 'timesheets') {
				$url = "http://localhost/api/timesheets/";
				// Forwarding on to the API.
				sendToAPI($inputdata, $url, $_SERVER["REQUEST_METHOD"], $oauthObj->access_token);
			} else {
				echo json_encode(array('error' => 'Connection Refused: API not specified.'));
			}
		} else {
			echo json_encode(array('error' => 'Connection Refused: Access token expired.'));
		}
		die;
	} elseif (!empty($oauthObj->access_token)) {
		echo json_encode(array('error' => 'Connection Refused: Invalid access token.'));
		die;
	}

	/* This section of code allows the user to generate a new access token by providing the client ID
	   client secret, and a refresh token. It also generates and passes back a new refresh token. */
	if (!empty($inputdata['client_id']) && !empty($inputdata['client_secret']) && !empty($inputdata['refresh_token'])) {
		if ($oauthObj->verifyClientIDAndSecret()) {
			if ($oauthObj->verifyRefreshToken()) {
				$oauthObj->generateAccessToken();
				$oauthObj->generateRefreshToken();
				$oauthObj->setAccessToken();
				$oauthObj->setRefreshToken();
				echo json_encode(array('access_token' => $oauthObj->access_token,
									'refresh_token' => $oauthObj->refresh_token));
			} else {
				echo json_encode(array('error' => 'Connection Refused: Invalid refresh token.'));
			}
		} else {
			echo json_encode(array('error' => 'Connection Refused: Invalid client ID/client secret combination.'));
		}
		die;
	}

	/* This section of code validates the user ID and password. If valid, then a new client ID and
	   client secret are generated and passed back to the application for later use in obtaining an
	   authorization code which should lead to the application getting a valid access token. */
	if (!empty($inputdata['user_id']) && !empty($inputdata['password'])) {
		if ($oauthObj->verifyLogonCreds()) {
			$oauthObj->generateClientID();
			$oauthObj->generateClientSecret();
			$oauthObj->setClientID();
			$oauthObj->setClientSecret();
			echo json_encode(array('client_id' => $oauthObj->client_id,
								'client_secret' => $oauthObj->client_secret));
		} else {
			echo json_encode(array('error' => 'Connection Refused: Invalid user ID/password combination.'));
		}
		die;
	}

	/* This section of code validates the client ID and client secret combination so that the application can
	   obtain a valid authorization code, which should then lead to the acquisition of a valid access token. */
	if (!empty($inputdata['client_id']) && !empty($inputdata['client_secret']) && empty($inputdata['refresh_token'])) {
		if ($oauthObj->verifyClientIDAndSecret()) {
			$oauthObj->generateAuthCode();
			$oauthObj->setAuthCode();
			echo json_encode(array('auth_code' => $oauthObj->auth_code));
		} else {
			echo json_encode(array('error' => 'Connection Refused: Invalid client ID/client secret combination.'));
		}
		die;
	}

	/* This section of code validates the client ID and authorization code combination. If valid, then the
	   application is issued a valid access token and a valid refresh token so that the API can be accessed. */
	if (!empty($inputdata['client_id']) && !empty($inputdata['auth_code'])) {
		if ($oauthObj->verifyClientIDAndAuthCode()) {
			$oauthObj->generateAccessToken();
			$oauthObj->generateRefreshToken();
			$oauthObj->setAccessToken();
			$oauthObj->setRefreshToken();
			echo json_encode(array('access_token' => $oauthObj->access_token,
								'refresh_token' => $oauthObj->refresh_token));
		} else {
			echo json_encode(array('error' => 'Connection Refused: Invalid client ID/authorization code combination.'));
		}	
		die;
	}
}

// If we make it to this line of code, something went terribly wrong.
echo json_encode(array('error' => 'Connection Refused: No valid credentials provided.'));
