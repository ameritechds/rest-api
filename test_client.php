<?php
// Use the login credentials to request a client ID and client secret.
// curl -H "Content-Type: application/json" -X POST http://localhost/api/authorize/ -d "user_id=testclient&password=testpass"
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/api/authorize");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "user_id=testclient&password=testpass");
$headers = array();
$headers[] = "Content-Type: application/x-www-form-urlencoded";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
$result=json_decode($result);
$client_id = $result->client_id;
$client_secret = $result->client_secret;

// Use the client ID and client secret to request and authorization code.
// curl -H "Content-Type: application/json" -X POST http://localhost/api/authorize/ -d "client_id=025c9150dd2006368799c3d7b6eb8dd2518295ff&client_secret=fcfb0239159bb67e2520df9df2ff80b3227f2600"
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/api/authorize");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=$client_id&client_secret=$client_secret");
$headers = array();
$headers[] = "Content-Type: application/x-www-form-urlencoded";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
$result=json_decode($result);
$auth_code = $result->auth_code;

// Use the authorization code and client ID to request an access token and a refresh token.
// curl -H "Content-Type: application/json" -X POST http://localhost/api/authorize/ -d "client_id=025c9150dd2006368799c3d7b6eb8dd2518295ff&auth_code=47f34964de59a953d369f8faf9484d3d152565f9"
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/api/authorize");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=$client_id&auth_code=$auth_code");
$headers = array();
$headers[] = "Content-Type: application/x-www-form-urlencoded";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
$result=json_decode($result);
$access_token = $result->access_token;
$refresh_token = $result->refresh_token;

// Use the access token in a GET request to get data back from the database.
// curl -H "Authorization: Bearer <Access Token>" http://localhost/api/customers
// curl -X GET -G -H "Authorization: Bearer <Access Token>" http://localhost/api/customers -d "id=28"
/* $ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/api/customers");
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_POSTFIELDS, "id=28");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$headers = array();
$headers[] = "Authorization: Bearer $access_token";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
echo $result; */

// Use the access token in a POST request to create a record in the database.
// curl -X POST -H "Authorization: Bearer <Access Token>" http://localhost/api/customers -d  "name=Joe Smith"
/* The following code can be used if data is coming from an input file:
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))                    
); */

/* $ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/api/customers");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "name=Gary Harris&address=123 Oak St.&city=Hickory");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$headers = array();
$headers[] = "Authorization: Bearer $access_token";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
echo $result; */

// Use the access token in a PUT request to update a record in the database.
// curl -X PUT -H "Authorization: Bearer <Access Token>" http://localhost/api/customers -d "id=28&name=Bill Jones"
/* $ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/api/customers");
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch, CURLOPT_POSTFIELDS, "id=108&name=Joe Smith");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$headers = array();
$headers[] = "Authorization: Bearer $access_token";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
echo $result; */

// Use the access token in a DELETE request to delete a record from the database.
// curl -X DELETE -H "Authorization: Bearer <Access Token>" http://localhost/api/customers -d "id=83"
/* $ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/api/customers");
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
curl_setopt($ch, CURLOPT_POSTFIELDS, "id=108");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$headers = array();
$headers[] = "Authorization: Bearer $access_token";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
echo $result; */

// cURL commands for the timesheet API.
// curl -X GET -G -H "Authorization: Bearer <Access Token>" http://localhost/api/timesheets
// curl -X GET -G -H "Authorization: Bearer <Access Token>" http://localhost/api/timesheets -d "id=297"
// curl -X POST -H "Authorization: Bearer <Access Token>" http://localhost/api/timesheets -d "username=Gary Harris&otherhours=2.0&empid=1234"
// curl -X PUT -H "Authorization: Bearer <Access Token>" http://localhost/api/timesheets -d "id=1391&username=Bill Jones&otherhours=2.0&empid=1234"
// curl -X DELETE -H "Authorization: Bearer <Access Token>" http://localhost/api/timesheets -d "id=1391"