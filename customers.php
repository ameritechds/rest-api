<?php

require_once 'classes/oauth.php';
require_once 'functions/header.php';
require_once 'functions/db.php';
require_once 'functions/json.php';

$oauthObj = new myOauth(); // Instantiate the main OAuth object.
if (!empty($_GET['access_token'])) {
	$oauthObj->access_token = $_GET['access_token'];
} elseif (!empty($_POST['access_token'])) {
	$oauthObj->access_token = $_POST['access_token'];
} elseif ($oauthObj->access_token = getBearerToken()) {
	// Everything is good. Keep moving.
} else {
	echo json_encode(array('error' => 'Connection Refused: No access token provided.'));
	die;
}

if (!empty($oauthObj->access_token) && $tokenObj = $oauthObj->getAccessTokenFromAccessToken($oauthObj->access_token)) {
	$today = date("Y-m-d H:i:s");
	$expire = $tokenObj[0]['expires'];
	$today_time = strtotime($today);
	$expire_time = strtotime($expire);
	if ($expire_time > $today_time) {
		// Continue on to the API code.
	} else {
		echo json_encode(array('error' => 'Connection Refused: Access token expired.'));
		die;
	}
} elseif (!empty($oauthObj->access_token)) {
	echo json_encode(array('error' => 'Connection Refused: Invalid access token.'));
	die;
}

$connection = dbConnect('taboplsk_dbtabo');

$request_method=$_SERVER["REQUEST_METHOD"];
switch($request_method) {
	case 'GET':
		// Retrieve Customers
		parse_str(file_get_contents("php://input"),$post_vars);
		if(!empty($post_vars["id"])) {
			$customer_id=intval($post_vars["id"]);
			get_customers($customer_id);
		} elseif (!empty($_POST["id"])) {
			$customer_id=intval($_POST["id"]);
			get_customers($customer_id);
		} elseif (!empty($_GET["id"])) {
			$customer_id=intval($_GET["id"]);
			get_customers($customer_id);
		} else {
			get_customers();
		}
		break;
	case 'POST':
		// Insert Customer
		$post_vars = file_get_contents("php://input"); // Check to see if data is coming from an input file.
		if (!empty($post_vars)) {
			if (isJSON($post_vars)) { // If JSON, then data is coming from a file and must be processed as JSON.
				$insertion_array = array();
				$post_vars = json_decode($post_vars);
				foreach($post_vars->data as $obj) {
					foreach($obj as $field => $value) {
						$insertion_array[$field] = $value;
					}
					if (!empty($insertion_array)) {
						insert_customer($insertion_array);
					}
				}
			} else { // Not JSON, so assume the data is coming from the command line, and parse it.
				parse_str($post_vars, $data);
				insert_customer($data);
			}
		} elseif (!empty($_POST)) {
			insert_customer($_POST);
		} elseif (!empty($_GET)) {
			insert_customer($_GET);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'No customer data provided.'
			);
			header('Content-Type: application/json');
			echo json_encode($response);
		}
		break;
	case 'PUT':
		// Update Customer
		$post_vars = file_get_contents("php://input"); // Check to see if data is coming from an input file.
		if (!empty($post_vars)) {
			if (isJSON($post_vars)) { // If JSON, then data is coming from a file and must be processed as JSON.
				$update_array = array();
				$post_vars = json_decode($post_vars);
				foreach($post_vars->data as $obj) {
					foreach($obj as $field => $value) {
							$update_array[$field] = $value;
					}
					if (!empty($update_array)) {
						update_customer($update_array);
					}
				}
			} else { // Not JSON, so assume the data is coming from the command line, and parse it.
				parse_str($post_vars, $data);
				update_customer($data);
			}
		} elseif (!empty($_POST)) {
			update_customer($_POST);
		} elseif (!empty($_GET)) {
			update_customer($_GET);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'No customer ID provided.'
			);
			header('Content-Type: application/json');
			echo json_encode($response);
		}
		break;
	case 'DELETE':
		// Delete Customer
		$post_vars = file_get_contents("php://input"); // Check to see if data is coming from an input file.
		if (!empty($post_vars)) {
			if (isJSON($post_vars)) { // If JSON, then data is coming from a file and must be processed as JSON.
				$update_array = array();
				$post_vars = json_decode($post_vars);
				foreach($post_vars->data as $obj) {
					foreach($obj as $field => $value) {
							$update_array[$field] = $value;
					}
					if (!empty($update_array)) {
						delete_customer($update_array);
					}
				}
			} else { // Not JSON, so assume the data is coming from the command line, and parse it.
				parse_str($post_vars, $data);
				delete_customer($data);
			}
		} elseif (!empty($_POST)) {
			delete_customer($_POST);
		} elseif (!empty($_GET)) {
			delete_customer($_GET);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'No customer ID provided.'
			);
			header('Content-Type: application/json');
			echo json_encode($response);
		}
		break;
	default:
		// Invalid Request Method
		header("HTTP/1.0 405 Method Not Allowed");
		break;
}

/* This function gets customer data from the database. It can retrieve a single customer based upon the customer ID.
   Or, it can retrieve all of the customers if the customer ID is not passed in. */
function get_customers($customer_id=0) {
	global $connection;
	try {
		$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		if($customer_id != 0) {
			$stmt = $connection->prepare("SELECT * FROM Customers WHERE ID = :id LIMIT 1");
			$stmt->bindParam(':id', $customer_id);
		} else {
			$stmt = $connection->prepare("SELECT * FROM Customers");
		}
		
		$response=array();
		if ($stmt->execute()) {
			$response = $stmt->fetchAll();
			header('Content-Type: application/json');
			echo json_encode($response);
		}
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}

/* This function creates new customers in the database. */
function insert_customer($data) {
	global $connection;
	try {
		$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$sql = 'INSERT INTO Customers (';
		
		foreach($data as $key => $value) { // Build the column list for the INSERT statement.
			if ($key != 'id') {
				$sql .= $key . ',';
			}
		}
		$sql = rtrim($sql,',');
		$sql .= ') VALUES (';

		foreach($data as $key => $value) { // Build the value list for the INSERT statement.
			if ($key != 'id') {
				$sql .= ':' . $key . ',';
			}
		}
		$sql = rtrim($sql,',');
		$sql .= ')';
		
		$stmt = $connection->prepare($sql);
										
		foreach($data as $key => $value) { // Build the variable bind list for the SQL statement.
			if ($key != 'id') {
				$stmt->bindParam(':'.$key, $data[$key]);
			}
		}
				
		if($stmt->execute()) {
			$response=array(
				'status' => 1,
				'status_message' =>'Customer Added Successfully.'
			);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'Customer Addition Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}

/* This function updates a customer's data in the database.*/
function update_customer($data) {
	global $connection;

	try {
		$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$sql = "UPDATE Customers SET ";
		
		foreach($data as $key => $value) { // Build the column = :key list for the UPDATE statement.
			if ($key != 'customer_id' && $key != 'id') {
				$sql .= $key . ' = ' . ':' . $key . ',';
			}
		}
		$sql = rtrim($sql,',');
		$sql .= " WHERE id = :id";
		
		$stmt = $connection->prepare($sql);
										
		foreach($data as $key => $value) { // Build the variable bind list for the SQL statement.
			$stmt->bindParam(':'.$key, $data[$key]);
		}
		
		if($stmt->execute()) {
			$response=array(
				'status' => 1,
				'status_message' =>'Customer Updated Successfully.'
			);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'Customer Update Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}

/* This function deletes a customer's data from the database. */
function delete_customer($data) {
	global $connection;
	try {
		$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt = $connection->prepare("DELETE FROM Customers WHERE ID = :id");
		$stmt->bindParam(':id', $data["id"]);
		if($stmt->execute()) {
			$response=array(
				'status' => 1,
				'status_message' =>'Customer Deleted Successfully.'
			);
		} else {
			$response=array(
				'status' => 0,
				'status_message' =>'Customer Deletion Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}
}
