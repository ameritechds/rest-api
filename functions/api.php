<?php

// The following function forwards the user on to the API, because they presented a valid access token.
function sendToAPI($data, $url, $request_method, $access_token) {
	$ch = curl_init();
	// curl_setopt($ch, CURLOPT_URL, $url . $access_token); // Build the URL.
	curl_setopt($ch, CURLOPT_URL, $url); // Build the URL.
	
	switch($request_method) {
		case 'GET':
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)); // Set the post data.
			break;
		case 'POST':
			curl_setopt($ch, CURLOPT_POST, 1); // This will be a POST request.
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)); // Set the post data.
			break;
		case 'PUT':
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)); // Set the post data.
			break;
		case 'DELETE':
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)); // Set the post data.
			break;
		default:
			echo 'Unknown request method...';
			die;
	}
		
	$headers = array();
	$headers[] = "Authorization: Bearer $access_token";
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, false); // No return transfer of data.
	curl_exec($ch);
	curl_close($ch);
}

